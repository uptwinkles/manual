# Uptwinkles communal house manual

This guide will tell you the ins and outs of the house. It is constantly growing and evolving, just like us, and we can change it if everyone in the house agrees to. In that sense, it's a living document, and it's meant to reflect the reality of the house right now rather than to enforce any sort of standard upon it.
