# Summary

* [Introduction](README.md)
* [Our values](values.md)
* [Doing chores](chores.md)
* [Paying rent](rent.md)
* [House rules](rules.md)
* [Making decisions](decisions.md)
