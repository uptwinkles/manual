# House Maintenance

The house is like a moving vehicle, and we are its gears. Humans aren't machines, though, so this comes with some challenges. One challenge is respecting peoples' individual freedom, while also retaining structure. As social anarchists, we strive to strike a balance, but as fast-moving visionaries with a mission, we have to put particular emphasis on structure. This means it takes a certain level of self-discipline and a deep understanding of our mission to cooperate with us, and everyone really needs to do their part.

## Goals of maintenance

The house should feel welcoming and inviting on a daily basis. This means it should be clean, well kept, and neatly organized. In the absence of this, our members will feel bogged down and it will affect our productivity and happiness.

Everyone is responsible for making this so, but we've agreed to divvy up the labor in a consistent way.

## Chore rotations

Most chores rotate on a weekly basis. That is, someone will be assigned to a chore for an entire week and they are expected to upkeep that chore during the week.

Two special chores, dinner and dishes, rotate every night. No one will ever be assigned to both at once.

If we have more people than chores, it means that sometimes people won't have to do any chores for a while. This is good because we get to live in a clean house without all the upkeep.

## Chore assignments

Chores are assigned to people automatically by the Uptwinkles API. The API is an open-source codebase written by members of Uptwinkles, and it can be modified by consensus and labor.

The current chore assignments can be viewed at any time by visiting the Uptwinkles dashboard: https://dashboard.uptwinkles.co/

Twink, our chatbot, will also remind us to do chores.
