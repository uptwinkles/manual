# Making decisions

Sometimes a decision needs to be made that will affect all members of the house. This could come up if:

* We want to use the house bank account to buy something outside of our agreed-upon budget.
* One of our chores or processes is inefficient and could be improved.
* Some people are annoyed by a particular behavior and want to ask to change it.
* We want to rearrange large pieces of furniture in the common areas.

The list above are just examples to give you a general idea. When in doubt: **test for consensus.** It is a grave error to attempt to bypass or subvert this system; it will be seen as a violation of trust and could be grounds for removal if your action was severe and cannot be easily reverted, eg spending a lot of house money on something other people don't want.

When we're faced with a house decision, we must form consensus before moving on. Consensus is a particular type of decision-making process which aims to get everyone on the same page. Please read [Shared Path, Shared Goal](https://www.sproutdistro.com/catalog/zines/organizing/shared-path-shared-goal/) for a complete understanding of the decision-making process. It's a short zine that should take you 10 minutes at most to read.

In order to test for consensus, someone has to present a proposal. If you personally wish to change something, you should be the one who creates that proposal. If you disagree with a proposal, you must work together with the proposer to rewrite the proposal. You have the right to block any proposal, but please realize that this is a very severe reaction that should only be in the best interest of the group, never selfishly. If you block a proposal, everyone will be looking at you to solve the problem at hand. You must take on that responsibility.

You can also abstain from voting on any decision, and you should abstain if the decision doesn't affect you. A proposal passes if every person either agrees or abstains.

If everyone can get in the same room together, that's great, and we can try to form consensus in person. Another good way to get quick consensus is to post in the Uptwinkles Matrix chatroom. This is fine for minor decisions, like "can so-and-so spend the night for three nights?"

More complicated or controversial proposals should be presented in the Uptwinkles Loomio group. This will give everyone the opportunity to review the proposal at their own pace, and respond in an organized way.
