rm -rf _book
./node_modules/.bin/gitbook build
git -C _book init
touch _book/.static
rm _book/package.json
git -C _book add --all
git -C _book commit -am "Dokku"
git -C _book remote add dokku dokku@uptwinkles.co:manual
git -C _book push -f dokku master
