# Rent

The total rent for Uptwinkles is $2,500 per month, and the cost of utilities and food varies.

There are two fees that members may be expected to pay each month: rent and a share fee. These fees are **due on the 15th of each month, prior to the month you're paying for.** For instance, if you are paying fees for September, they are due by August 15th. This is done to give adequate time for us to deliver rent to the landlord, as well as time for the house to adapt if anything goes wrong.

Fees should be delivered electronically to one of the bank-holders through an agreed upon method.

## Paying on time

It is important that rent is sent to a bank-holder on or before the 15th of the month, otherwise it may not be delivered to the landlord on time. The landlord receives a cashier's check by mail, which takes 7+ days to arrive. Therefore, all rent money must be in the bank account at least 7 days prior to the 1st of the month.

In order to transfer money you send to a bank-holder into the Uptwinkles account, it could require 3-5 extra days of processing. Therefore, the bank-holder must receive your rent at least 12 days (7 + 5) before rent is due. We've settled on 15 days to allow a little wiggle-room in case of an issue with the process. It's therefore necessary that members pay on or by the 15th of each month.

## Cost of rooms

There is a cost associated with each of the house's 5 rooms. We have mutually agreed upon these values based on the size of the room, the room's bathroom situation, the location of the room, and any other qualities that affect the room's desirability.

The current cost of each bedroom is as follows:

* 2nd floor - Deck room - $492/mo
* 2nd floor - Small room - $380/mo
* 2nd floor - Front room - $492/mo
* 3rd floor - Top room - $492/mo
* 3rd floor - Master bedroom - $644/mo

## Share fee

Each person living in the house is expected to pay a base fee of **$200/mo** regardless of which bedroom they are staying in.

The share fee is used to pay for all utilities, including electricity, gas, water, cleaning supplies, and our communal food system.

## House bank account

All money goes into the house bank account. After a member lives at Uptwinkles for 1 year, they are eligible to receive a bank card. Bank statements should be sent to all members at the end of each month for transparency.

When funds are in excess, the group may also decide to use this money to improve the house by purchasing shared furniture, activism materials, and shared equipment. However, it is also smart to keep money saved in case something goes wrong and we need time to recover.
