# House Rules

## Financial (Rent and other bills)

1. Rent is due on the 15th of each month.
2. Food and supplies (including toilet paper, paper towels, soap, etc) are purchased as a group and distributed as needed.

## New Roommates

1. New roommates should be interviewed carefully.
2. Current roommates must reach consensus to allow a new roommate in.
3. All else being equal, preference (if given a choice) is given to activists, women, POC, immigrants, and queer people.
4. A potential tenant should exhibit qualities of a humble, empathetic, but exceptionally knowledgeable individual.
5. Prospective tenants must pay first month's rent, last month's rent, and their first share fee for their spot to be secured.
7. Prospective tenants must sign an agreement.

## Non-humans

1. Non-humans living in the house are also our roommates and should be treated with the same courtesy and respect as humans.
2. Non-humans should be considered during consensus votes.
3. The house can be used to temporarily house animals rescued by Liberation Philly at any time upon the agreement of a safety plan for the animal and the current non-human tenants.
4. It is the responsibility of a non-human’s guardian to take care of them, clean up after them, and feed them.

## Culture

1. Consensus of all relevant people must formed before acting.
2. This is a vegan house.
3. Animal products are not allowed into the house. Except cat food, but we hate it.
4. Liberation Philly may use this house as a community space for events.
5. We should be willing to put aside our personal desires for the sake of altruism.
6. No one should be required to use software that doesn't respect their freedom or privacy in order to participate in the house.
7. House disagreements should be handled through [non-violent communication](http://www.nonviolentcommunication.com/pdf_files/4part_nvc_process.pdf).
8. Relationships may not be verbally, physically, emotionally, or sexually abusive. Consent is mandatory at all times.
9. Racism, sexism, homophobia, and speciesism are not tolerated.
10. Violence against humans and non-humans is not tolerated.

## Chores

1. Chores should be done.
2. Do your chores.
3. You can ask someone else to do your chores, but you ought to do their chores in exchange.

## Safety

1. When exiting the front door, press the lock button and wait to ensure the lock has fully engaged.
2. When pulling out of the garage, wait until the garage door is entirely closed before driving away.
3. Smoke:
  * Vapor is allowed, but smoke to be inhaled is not allowed in the house.
  * Smoking should be done on the deck, outside, or carefully out a bedroom window to prevent smells from remaining in the house.
  * Candles and incense are allowed under supervised use.

## Courtesy

1. Noise
  * Quiet hours from 10pm - 9am on weeknights, 12pm 10am on weekends. Quiet hours may be waved on any given day with consensus (ex for event, party, or guests).
  * Please be mindful of volume in the common area at night.
  * If you're doing something loud in your room (ex. blasting music) please close your bedroom door.
  * Indoor voices should be used at all times when indoors.
2. Ask before borrowing any personal items.
3. When someone sneezes, say “kombucha” and not “bless you.”

## Guests

1. Non-overnight guests
  * 1 or 2 non-overnight guests have no requirement, but no one will mind a heads-up.
  * 3-5 non-overnight guests requires a courtesy heads-up.
  * 6+ non-overnight guests constitutes a party and requires consensus.
2. Romantic partners
  * ???
