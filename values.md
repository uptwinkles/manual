# Values

Uptwinkles is an intentional community centered around a shared set of values.

## Our mission

Uptwinkles aims to provide a consensus-based community space for the animal rights community in Philadelphia to grow.

## Group values

1. **Equality.** Every member has equal decision making power.
2. **Internal Transparency.** Every member of the group should have access to the decisions we've made. A monopoly on information creates a monopoly on decision making power.
3. **Respect.** Every member respects one another. We acknowledge that we all have different backgrounds, skills, and opinions. These differences make us stronger.
4. **Teach Each Other.** As a group, we are stronger when we share our knowledge. When skills are not shared, it creates bottlenecks on what we can accomplish and single points of failure.
